package com.lanna.myandengine;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;

/**
 * Created by lanna on 4/4/15.
 *
 */
public class GameActivity extends BaseGameActivity {

    // new stuff here
    SceneManager sceneManager;
    Camera mCamera;

    @Override
    public EngineOptions onCreateEngineOptions() {
        // modification here:
        mCamera = new Camera(0, 0, Constants.CAMERA_WIDTH, Constants.CAMERA_HEIGHT);

        return new EngineOptions(true, ScreenOrientation.PORTRAIT_FIXED,
                new RatioResolutionPolicy(Constants.CAMERA_WIDTH, Constants.CAMERA_HEIGHT), mCamera);
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws Exception {
        sceneManager = new SceneManager(this, mEngine, mCamera);
        sceneManager.loadGameResources();
        // let the game engine know we are done loading the resources we need.
        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }

    @Override
    public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws Exception {
        pOnCreateSceneCallback.onCreateSceneFinished(sceneManager.createSplashScene());
    }

    @Override
    public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
//        mEngine.registerUpdateHandler(new TimerHandler(3f,
//                new ITimerCallback() {
//                    @Override
//                    public void onTimePassed(TimerHandler pTimerHandler) {
//                        mEngine.unregisterUpdateHandler(pTimerHandler);
//                        sceneManager.createMenuScene();
//                        sceneManager.setCurrentScene(SceneManager.AllScenes.MENU);
//                    }
//                }));

        pOnPopulateSceneCallback.onPopulateSceneFinished();
    }
}
