package com.lanna.myandengine;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by lanna on 4/4/15.
 *
 */
public class BallSprite extends AnimatedSprite {

    private static final boolean IS_LIMIT_TOP 			= true;
    private static final boolean IS_LIMIT_LEFT_RIGHT 	= true;

    private boolean isThrow = false;
    private float timeDown = 0;
    private double alpha = 0;
    private double v = 0;
    private float x0 = 0;
    private float y0 = 0;
    private byte directionX = 1; // 1: right, -1: left

    private double v2 = 0;
    private double tana = 0;
    private double sina2 = 0;
    private double doubleV2Cosa2 = 0;

    private float highRange;

    public BallSprite(final float pX, final float pY,
                      final TiledTextureRegion pTextureRegion,
                      final VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);

        x0 = pX;
        y0 = pY;
        setScale(0.2f);

//		Log.d("lanna", String.format("Input: x0=%.2f, y0=%.2f, MAX_X=%.2f, MAX_Y=%.2f", x0, y0, MAX_X, MAX_Y));
    }

    public void setOriginPosition(float x0, float y0) {
        resetValues();
        this.x0 = limitX(x0);
        this.y0 = y0;
        setPosition(this.x0, this.y0);
    }

    private void resetValues() {
        isThrow = false;
        timeDown = 0;
        realTime = 0;
    }

    // Handle ball touch event
    private float eventX0, eventY0, eventTimeStart;
    @Override
    public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
//			String location = pTouchAreaLocalX + ", " + pTouchAreaLocalY;
        switch (pSceneTouchEvent.getAction()) {
            case TouchEvent.ACTION_DOWN:
//				Log.i("lanna", "Touch ACTION_DOWN " + location);
                x0 = getX();
                y0 = getY();
                eventX0 = pTouchAreaLocalX;
                eventY0 = pTouchAreaLocalY;
                eventTimeStart = System.nanoTime();
                break;
            case TouchEvent.ACTION_UP:
//				Log.i("lanna", "Touch ACTION_UP " + location);
                startThrow(pTouchAreaLocalX - eventX0, pTouchAreaLocalY - eventY0, Constants.calcSecondElapsed(eventTimeStart));

//				addLine(getLongRange()+getWidth()/2, 0, 0, CAMERA_HEIGHT, Color.BLUE); // x max
//				addLine(0, getHighRange()+getHeight()/2, CAMERA_WIDTH, 0, Color.BLUE); // y max
                break;

            default:
                break;
        }
        return true;
    }

    // TODO start throw the ball
    private static final float V_RATE 			= 0.5f;
    private static final float V_RATE_FALLING	= 0.8f;

//	private static final byte D_DELTA	= 2;

    private double timeRate;
    private float realTime;

    public void startThrow(float dx, float dy, float dt) {
        if (Math.abs(dy) < 10 || (isOutOfScreenWidth(mX + dx))) {
            return;
        }

        resetValues();

        directionX = (byte) ((dx > 0) ? 1 : -1);

        dx = Math.abs(dx);// if (dx < D_DELTA)	dx = D_DELTA;
        dy = Math.abs(dy);// if (dy < D_DELTA)	dy = D_DELTA;

        v = V_RATE * Math.sqrt(dx*dx + dy*dy) / dt; // v=s/t
        alpha = Math.atan(dy/dx); // (dx == 0) ? (Math.PI/2) : Math.atan(dy/dx); // Math.PI/3; // 60o //
        timeRate = (90f - Math.toDegrees(alpha)) / 250f;//(Math.abs(90 - alphaDegree) < 20) ? 0.02f : (x)
//		Log.d("lanna", String.format("v0=%.2f, alpha=%.0fo, timeRate=%.2f, dt=%.2f", v, Math.toDegrees(alpha), timeRate, dt));
        v = limitV(v, alpha);
//		Log.d("lanna", String.format("v=%.2f", v));

        // calculate some constants in formula to help reduce performance to calculate values (such as Y value)
        v2 = v*v; // v2
        tana = Math.tan(alpha); // tan
        sina2 = Math.sin(alpha); sina2 *= sina2; // sin2
        doubleV2Cosa2 = Math.cos(alpha); doubleV2Cosa2 *= 2*v2* doubleV2Cosa2; // 2v2cos2

        highRange = getHighRange();
        isThrow = true;

//		Log.i("lanna", String.format("=> dx=%.2f, dy=%.2f, dt=%.2f -> v=%.2f, alpha=%.2fPI (%.0fo)", dx, dy, dt, v, alpha, Math.toDegrees(alpha)));
//		Log.i("lanna", String.format("=> cosa=%.2f, sina=%.2f, tana=%.2f", Math.cos(alpha), Math.sin(alpha), tana));
//		Log.i("lanna", String.format("=> HighRange=%.2f, LongRange=%.2f", getHighRange(), getLongRange()));
    }

    private double limitV(double v, double alpha) {
        // MIN_V
//		v = Math.max(v, 80);

        // MAX_V
        if (IS_LIMIT_TOP) {
            v = Math.min(v, calcMaxVToLimitTop(alpha));
        }
        if (IS_LIMIT_LEFT_RIGHT) {
//			Log.i("lanna", String.format("=> directionX=%d, calcMaxVToLimitLeftRight=%.2f", directionX, calcMaxVToLimitLeftRight(alpha, directionX)));
            v = Math.min(v, calcMaxVToLimitLeftRight(alpha, directionX));
        }
        return v;
    }

    // calc maxV to make sure that HighRange not larger than CAMERA_HEIGHT
    private float calcMaxVToLimitTop(double alpha) {
        return (float) (Math.sqrt(2*Constants.G*y0) / Math.abs(Math.sin(alpha)));
    }

    /*
     * calc maxV to make sure that LongRange not larger than CAMERA_WIDTH (directionX = 1) and not smaller than 0 (directionX = -1)
     * longRange is v2*Math.sin(2*alpha)/Constants.G (without x0 and directionX)
     * then maxV in this case is: maxV <= (directionX < 0 ? x0 : CAMERA_WIDTH-x0) * G/sin2a
     */
    private float calcMaxVToLimitLeftRight(double alpha, byte directionX) {
        return (float) (((directionX < 0 ? x0 : Constants.MAX_X-x0) * Constants.G) / Math.abs(Math.sin(2*alpha)));
    }

    // TODO Routes-in-fly of ball is calculated by formula:
    // x = v0 * t
    // y = -g.x2/(2.v02.cos2(a))+(tan(a).x)
    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        if (isThrow) {
            // get time stamp
//			float realTime = Constants.calcSecondElapsed(tStart);
            pSecondsElapsed += timeRate;
            realTime += pSecondsElapsed;

            float x = 0;
            float y = 0;

            // calculate X, Y
            if (timeDown != 0) {
                x = getX(); // same x when down
                y = getY() + 8 + (realTime - timeDown); // y down
            } else {
                x = calcX(realTime);
                y = calcY(Math.abs(x));
                x += x0;
                y += y0;
            }

//            if (x < 0 || x > MAX_X || y < 0 || y > MAX_Y) {
//                if (x < 0)			x = 0;
//                else if (x > MAX_X)	x = MAX_X;
//                if (y < 0)			y = 0;
//                else if (y > MAX_Y)	y = MAX_Y;
//                isThrow = false;
//            }
            if (isOutOfScreenWidth(x)) {
                x = limitX(x);
                timeDown = realTime;
            }
            if (isOutOfScreenHeight(y)) {
                if (y > Constants.MAX_Y) y = Constants.MAX_Y;
                isThrow = false;
            }

            if (v < V_RATE_FALLING && y < highRange) { // cause Oy is reversed
                v = V_RATE_FALLING;
            }
            setX(x);
            setY(y);
//			Log.i("lanna", String.format("t=%.2f: x=%.2f, y=%.2f", realTime, x, y));
            super.onManagedUpdate(pSecondsElapsed);
        }
    }

    private float limitX(float x) {
        if (x < 0)		        return 0;
        if (x > Constants.MAX_X)return Constants.MAX_X;
        return x;
    }

    private boolean isOutOfScreenWidth(float x) {
        return (IS_LIMIT_LEFT_RIGHT && (x < 0 || x > Constants.MAX_X));
    }

    private boolean isOutOfScreenHeight(float y) {
        return (IS_LIMIT_TOP && y < -Constants.BALL_SIZE) || y > Constants.MAX_Y; // y
    }

    // x = v0*t
    private float calcX(float t) {
        return (float) (directionX*v*t);
    }

    // y = (-g.x2/(2.v02.cos2(a))) + (tan(a).x)
    private float calcY(float x) {
        return -(float) (-Constants.G*x*x/ doubleV2Cosa2 + tana*x); // use "-" because the Oy is top-bottom that reverse from normal bottom-top
    }

    // H = (v02.sin2(a))/2.g
    public float getHighRange() {
        return (float) (y0 - v2*sina2/(2f*Constants.G)); // use "y0 - " because the Oy is top-bottom that reverse from normal bottom-top
    }

    // L = (v02.sin(2.a))/2g
//    public float getLongRange() {
//        return (float) (x0 + directionX*v2*Math.sin(2*alpha)/Constants.G); // x0 and directionX: is for calc real value in screen
//    }
}