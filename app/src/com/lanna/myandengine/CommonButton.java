package com.lanna.myandengine;

import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.HorizontalAlign;

/**
 * Created by lanna on 4/25/15.
 *
 */
public class CommonButton extends ButtonSprite {

    public CommonButton(VertexBufferObjectManager pVertexBufferObjectManager,
                        TextureRegion pTextureRegion, Font pFont, String pText) {
        super(0, 0, pTextureRegion, pVertexBufferObjectManager);
        Text buttonText = new Text(0, 0, pFont, pText, new TextOptions(HorizontalAlign.CENTER), pVertexBufferObjectManager);
        buttonText.setPosition((this.getX() + this.getWidth() - buttonText.getWidth()) / 2, (this.getY() + this.getHeight() - buttonText.getHeight()) / 2);
        buttonText.setColor(1, 0, 0);
        this.setAlpha(1);
        this.attachChild(buttonText);
    }


}
