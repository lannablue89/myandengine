package com.lanna.myandengine;

import android.graphics.Color;

import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.ui.activity.BaseGameActivity;

/**
 * Created by lanna on 4/5/15.
 *
 */

public class AllRes {

    public static final float DEFAULT_TEXT_SIZE = 25;

    public enum AppFont {

        roboto_medium("Roboto-Medium.ttf"),
        ;

        // define Font values
        String fontName;
        Font font;
        AppFont(String fontName) {
            this.fontName = fontName;
        }

        public static void loadAll(BaseGameActivity activity) {
            FontFactory.setAssetBasePath("font/");
            final ITexture fontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

            for (AppFont font : AppFont.values()) {
                font.font = FontFactory.createFromAsset(activity.getFontManager(), fontTexture, activity.getAssets(), font.fontName, DEFAULT_TEXT_SIZE, true, Color.WHITE);
                font.font.load();
            }
        }
    }

    public enum AppImage {

        bg_in_game("bg_in_game.png", 540, 844),
        bg_pre_game("bg_pre_game.png", 540, 960),
        btn_play("btn_play.png", 130, 72),
        btn_scores("btn_scores.png", 128, 72),
        btn_solid("btn_solid.png", 130, 72), // red + corner
        ic_ball("ic_ball.png", 190, 190),
        ic_basket("ic_basket.png", 256, 256),
        logo_1("logo_1.png", 284, 470),
        logo_2("logo_2.png", 216, 97),
        logo_title("logo_title.png", 147, 60),
        ;

        // define Image values
        public String resName;
        public int w, h;
        public ITextureRegion textureRegion;

        AppImage(String resName, int w, int h) {
            this.resName = resName;
            this.w = w;
            this.h = h;
        }

        @Override
        public String toString() {
            return resName;
        }

        public static void loadAll(BaseGameActivity activity) {
            BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
            BitmapTextureAtlas bitmapTextureAtlas;
            for (AppImage imgRes : AppImage.values()) {
                bitmapTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), imgRes.w, imgRes.h);
                imgRes.textureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bitmapTextureAtlas, activity, imgRes.resName, 0, 0);
                bitmapTextureAtlas.load();
            }
        }

    }
}
