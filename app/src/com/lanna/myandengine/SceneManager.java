package com.lanna.myandengine;

import android.util.Log;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.ui.activity.BaseGameActivity;

/**
 * Created by lanna on 4/4/15.
 *
 */
public class SceneManager implements ButtonSprite.OnClickListener, IOnAreaTouchListener {

    public enum AllScenes {
        SPLASH, MENU, GAME, END_GAME, OPTIONS
    }

    private AllScenes currentScene;
    private BaseGameActivity activity;
    private Engine engine;
    private Camera camera;
    private Scene splashScene, menuScene, gameScene;

    // splash scene
    private ButtonSprite btnPlay, btnScores;

    // menu scene
    private ButtonSprite btnOption, btnComeBack;

    public SceneManager(BaseGameActivity act, Engine eng, Camera cam) {
        activity = act;
        engine = eng;
        camera = cam;
    }

    // ===========================================
    // Load Resources
    // ===========================================
    public void loadGameResources() {
        AllRes.AppImage.loadAll(activity);
        AllRes.AppFont.loadAll(activity);
//        AllRes.AppSound.loadAll(activity);
    }


    public AllScenes getCurrentScene() {
        return currentScene;
    }

    public void setCurrentScene(AllScenes currentScene) {
        this.currentScene = currentScene;
        switch (currentScene) {
            case SPLASH:
                break;
            case MENU:
                engine.setScene(menuScene);
                break;
            case GAME:
                engine.setScene(gameScene);
                break;

            default:
                break;
        }
    }

    // ===========================================
    // Splash Scene: btnPlay, btnHighScores
    // ===========================================
    public Scene createSplashScene() {
        splashScene = new Scene();
        splashScene.setTouchAreaBindingOnActionDownEnabled(true);

        // bg
        Sprite bgSprite = new Sprite(0, 0, AllRes.AppImage.bg_pre_game.textureRegion, engine.getVertexBufferObjectManager());
        bgSprite.setPosition((camera.getWidth() - bgSprite.getWidth()) / 2, (camera.getHeight() - bgSprite.getHeight())); // bg full screen
        splashScene.attachChild(bgSprite);

        // buttons: play, scores
        btnPlay = new ButtonSprite(0, 0, AllRes.AppImage.btn_play.textureRegion, engine.getVertexBufferObjectManager());
        btnPlay.setPosition(50, camera.getHeight() - btnPlay.getHeight() - 100);
        btnPlay.setScale(0.9f);
        btnPlay.setOnClickListener(this);
        splashScene.attachChild(btnPlay);
        splashScene.registerTouchArea(btnPlay);

        btnScores = new ButtonSprite(0, 0, AllRes.AppImage.btn_scores.textureRegion, engine.getVertexBufferObjectManager());
        btnScores.setPosition(camera.getWidth() - btnScores.getWidth() - 50, camera.getHeight() - btnScores.getHeight() - 100);
        btnScores.setScale(0.9f);
        btnScores.setOnClickListener(this);
        splashScene.attachChild(btnScores);
        splashScene.registerTouchArea(btnScores);

        // logo dunk
        Sprite logoSprite = new Sprite(0, 0, AllRes.AppImage.logo_1.textureRegion, engine.getVertexBufferObjectManager());
        logoSprite.setPosition((camera.getWidth() - logoSprite.getWidth()) / 2, (camera.getHeight() - btnPlay.getY() - btnPlay.getHeight()) / 2);
        logoSprite.setScale(0.7f);
        splashScene.attachChild(logoSprite);

        return splashScene;
    }

    // ===========================================
    // Menu Scene: Tuy Chon, Hoan Lai
    // ===========================================
    public Scene createMenuScene() {
        menuScene = new Scene();
        menuScene.setBackground(new Background(0, 0, 0));

        Sprite icon = new Sprite(0, 0, AllRes.AppImage.bg_pre_game.textureRegion, engine.getVertexBufferObjectManager());
        icon.setPosition((camera.getWidth() - icon.getWidth()) / 2, (camera.getHeight() - icon.getHeight()) / 2);
        menuScene.attachChild(icon);

        // buttons: btnOption, btnComeBack
        btnOption = new CommonButton(engine.getVertexBufferObjectManager(), (TextureRegion) AllRes.AppImage.btn_solid.textureRegion,
                AllRes.AppFont.roboto_medium.font, "Tuy Chon");
        btnOption.setPosition(50, camera.getHeight() - btnOption.getHeight() - 100);
        btnOption.setOnClickListener(this);
        menuScene.attachChild(btnOption);
        menuScene.registerTouchArea(btnOption);

        btnComeBack = new CommonButton(engine.getVertexBufferObjectManager(), (TextureRegion) AllRes.AppImage.btn_solid.textureRegion,
                AllRes.AppFont.roboto_medium.font, "Hoan Lai");
        btnComeBack.setPosition(camera.getWidth() - btnComeBack.getWidth() - 50, camera.getHeight() - btnComeBack.getHeight() - 100);
        btnComeBack.setOnClickListener(this);
        menuScene.attachChild(btnComeBack);
        menuScene.registerTouchArea(btnComeBack);

        return menuScene;
    }

    // ===========================================
    // Game Scene
    // ===========================================
    public Scene createGameScene() {
        return null;
    }

    // ===========================================
    // Game Result
    // ===========================================
    public Scene createGameResultScene() {
        return null;
    }

    // ===========================================
    // Game Result
    // ===========================================
    public Scene createOptionScene() {
        return null;
    }



    // ===========================================
    // Game Events
    // ===========================================

    @Override
    public void onClick(final ButtonSprite pButtonSprite, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
        if (pButtonSprite == btnPlay) {
            Log.i("ui", "btnPlay click");
            createMenuScene();
            setCurrentScene(AllScenes.MENU);
        }
        else if (pButtonSprite == btnScores) {
            Log.i("ui", "btnScores click");
        }
        else if (pButtonSprite == btnOption) {
            Log.i("ui", "btnOption click");
        }
        else if (pButtonSprite == btnComeBack) {
            Log.i("ui", "btnComeBack click");
        }
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, ITouchArea pTouchArea, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if (pTouchArea.equals(btnPlay)) {
            Log.i("ui", "btnPlay touch");
            return true;
        }
        return true;
    }

}

