package com.lanna.myandengine;

/**
 * Created by lanna on 3/29/15.
 *
 */
public class Constants {

	public static final int CAMERA_WIDTH 	= 480;
	public static final int CAMERA_HEIGHT 	= 720;

	public static final int BALL_SIZE = 190;
	public static final float MAX_X	= CAMERA_WIDTH - BALL_SIZE;
	public static final float MAX_Y	= CAMERA_HEIGHT - BALL_SIZE;

	public static final int G = 10; // GRAVITY 350; // adjusted constant (normal: g ~ 10 m/s2, bo qua luc. can? cua khong khi)
	public static final long SECOND_IN_NANO = (long) Math.pow(10, 9);

	public static float calcSecondElapsed(float tStart) {
		return ((System.nanoTime() - tStart) / Constants.SECOND_IN_NANO);
	}

}
